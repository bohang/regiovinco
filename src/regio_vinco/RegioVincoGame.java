package regio_vinco;

import audio_manager.AudioManager;
import java.util.concurrent.locks.ReentrantLock;
import javafx.event.EventType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import pacg.PointAndClickGame;
import static regio_vinco.RegioVinco.*;

/**
 * This class is a concrete PointAndClickGame, as specified in The PACG
 * Framework. Note that this one plays Regio Vinco.
 *
 * @author McKillaGorilla
 */
public class RegioVincoGame extends PointAndClickGame {

    // THIS PROVIDES GAME AND GUI EVENT RESPONSES
    RegioVincoController controller;

    // THIS PROVIDES MUSIC AND SOUND EFFECTS
    AudioManager audio;
    
    // THESE ARE THE GUI LAYERS
    Pane backgroundLayer;
    Pane gameLayer;
    Pane guiLayer;
    Label RegionsFound=new Label();
    Label RegionsLeft=new Label();
    Label IncorrectGuesses=new Label();
    Label winRegion=new Label();
    Label winScore =new Label();
    Label winGameDuration=new Label();
    Label winSubRegions=new Label();
    Label winIncorrectGuess=new Label();
    ReentrantLock myLock =new ReentrantLock();
    
    
    
    
   
    

    /**
     * Get the game setup.
     */
    
    public RegioVincoGame(Stage initWindow) {
	super(initWindow, APP_TITLE, TARGET_FRAME_RATE);
	initAudio();
    }
    
    public AudioManager getAudio() {
	return audio;
    }
    
    public Pane getGameLayer() {
	return gameLayer;
    }

    /**
     * Initializes audio for the game.
     */
    private void initAudio() {
	audio = new AudioManager();
	try {
	    audio.loadAudio(TRACKED_SONG, TRACKED_FILE_NAME);
	    audio.play(TRACKED_SONG, true);

	    audio.loadAudio(AFGHAN_ANTHEM, AFGHAN_ANTHEM_FILE_NAME);
	    audio.loadAudio(SUCCESS, SUCCESS_FILE_NAME);
	    audio.loadAudio(FAILURE, FAILURE_FILE_NAME);
	} catch (Exception e) {
	    
	}
    }

    // OVERRIDDEN METHODS - REGIO VINCO IMPLEMENTATIONS
    // initData
    // initGUIControls
    // initGUIHandlers
    // reset
    // updateGUI
    /**
     * Initializes the complete data model for this application, forcing the
     * setting of all game data, including all needed SpriteType objects.
     */
    @Override
    public void initData() {
	// INIT OUR DATA MANAGER
	data = new RegioVincoDataModel();
	data.setGameDimensions(GAME_WIDTH, GAME_HEIGHT);
	boundaryLeft = 0;
	boundaryRight = GAME_WIDTH;
	boundaryTop = 0;
	boundaryBottom = GAME_HEIGHT;
    }

    /**
     * For initializing all GUI controls, specifically all the buttons and
     * decor. Note that this method must construct the canvas with its custom
     * renderer.
     */
    @Override
    public void initGUIControls() {
	// LOAD THE GUI IMAGES, WHICH INCLUDES THE BUTTONS
	// THESE WILL BE ON SCREEN AT ALL TIMES
	backgroundLayer = new Pane();
	addStackPaneLayer(backgroundLayer);
	addGUIImage(backgroundLayer, BACKGROUND_TYPE, loadImage(BACKGROUND_FILE_PATH), BACKGROUND_X, BACKGROUND_Y);
  
	
	// THEN THE GAME LAYER
	gameLayer = new Pane();
	addStackPaneLayer(gameLayer);
	
	// THEN THE GUI LAYER
	guiLayer = new Pane();
	addStackPaneLayer(guiLayer);
	addGUIImage(guiLayer, TITLE_TYPE, loadImage(TITLE_FILE_PATH), TITLE_X, TITLE_Y);
	addGUIButton(guiLayer, START_TYPE, loadImage(START_BUTTON_FILE_PATH), START_X, START_Y);
	addGUIButton(guiLayer, EXIT_TYPE, loadImage(EXIT_BUTTON_FILE_PATH), EXIT_X, EXIT_Y);
        ImageView provinceimage = addGUIImage(guiLayer, PROVINCE_IMAGE, loadImage(PROVINCE_IMAGE_FILE_PATH), 900 , PROVINCE_Y);
        provinceimage.setVisible(false); 
        
	// NOTE THAT THE MAP IS ALSO AN IMAGE, BUT
	// WE'LL LOAD THAT WHEN A GAME STARTS, SINCE
	// WE'LL BE CHANGING THE PIXELS EACH TIME
	// FOR NOW WE'LL JUST LOAD THE ImageView
	// THAT WILL STORE THAT IMAGE
	ImageView mapView = new ImageView();
	mapView.setX(MAP_X);
	mapView.setY(MAP_Y);
	guiImages.put(MAP_TYPE, mapView);
	guiLayer.getChildren().add(mapView);
	// NOW LOAD THE WIN DISPLAY, WHICH WE'LL ONLY
	// MAKE VISIBLE AND ENABLED AS NEEDED
	ImageView winView = addGUIImage(guiLayer, WIN_DISPLAY_TYPE, loadImage(WIN_DISPLAY_FILE_PATH), WIN_X, WIN_Y);
	winView.setVisible(false);
         
        
    }
    
    // HELPER METHOD FOR LOADING IMAGES
    private Image loadImage(String imagePath) {	
	Image img = new Image("file:" + imagePath);
	return img;
    }

    /**
     * For initializing all the button handlers for the GUI.
     */
    @Override
    public void initGUIHandlers() {
	controller = new RegioVincoController(this);

	Button startButton = guiButtons.get(START_TYPE);
        startButton.setStyle("-fx-background-color:black;");
	startButton.setOnAction(e -> {
            ImageView provinceimage = guiImages.get(PROVINCE_IMAGE);
            provinceimage.setVisible(true);
            RegionsFound=addGuiLabel(REGIONSFOUND,RegionsFound,REGIONFOUND_X,REGIONLEFT_Y);
            guiLayer.getChildren().add(RegionsFound);
            RegionsFound.setVisible(true);
            RegionsLeft=addGuiLabel(REGIONSLEFT,RegionsLeft,REGIONLEFT_X,REGIONLEFT_Y);
            guiLayer.getChildren().add(RegionsLeft);
            RegionsLeft.setVisible(true);
            IncorrectGuesses=addGuiLabel(INCORRECTGUESSES,IncorrectGuesses,INCORRECTGUESSES_X,INCORRECTGUESSES_Y);
            guiLayer.getChildren().add(IncorrectGuesses);
            IncorrectGuesses.setVisible(true);
	    controller.processStartGameRequest();
            ImageView map = guiImages.get(MAP_TYPE);
            map.setVisible(true);
            ((RegioVincoDataModel)data).returnTimer().setVisible(true);
             winRegion=addGuiLabel2(WIN_REGION,winRegion,450,295);
             guiLayer.getChildren().add(winRegion);
             winScore=addGuiLabel2(WIN_SCORE, winScore, 440,325);
             guiLayer.getChildren().add(winScore);
             winGameDuration=addGuiLabel2(GAME_DURATION,winGameDuration,450, 355);
             guiLayer.getChildren().add(winGameDuration);
             winSubRegions=addGuiLabel2(GAME_SUBREGION,winSubRegions,445, 385);
             guiLayer.getChildren().add(winSubRegions);
             winIncorrectGuess=addGuiLabel2(GAME_INCORRECTGUESS, winIncorrectGuess, 450, 415);
             guiLayer.getChildren().add(winIncorrectGuess);
             winRegion.setVisible(false);
             winScore.setVisible(false);
             winGameDuration.setVisible(false);
             winSubRegions.setVisible(false);
             winIncorrectGuess.setVisible(false);
             
            

	});

	Button exitButton = guiButtons.get(EXIT_TYPE);
	exitButton.setStyle("-fx-background-color:black;");
        exitButton.setOnAction(e -> {
      
	    controller.processExitGameRequest();
	});

	// MAKE THE CONTROLLER THE HOOK FOR KEY PRESSES
	keyController.setHook(controller);

	// SETUP MOUSE PRESSES ON THE MAP
	ImageView mapView = guiImages.get(MAP_TYPE);
	mapView.setOnMousePressed(e -> {
	    controller.processMapClickRequest((int) e.getX(), (int) e.getY());
            controll();
            
            
	});
	
	// KILL THE APP IF THE USER CLOSES THE WINDOW
	window.setOnCloseRequest(e->{
	    controller.processExitGameRequest();
	});
    
    }

    /**
     * Called when a game is restarted from the beginning, it resets all game
     * data and GUI controls so that the game may start anew.
     */
    @Override
    public void reset() {
	// IF THE WIN DIALOG IS VISIBLE, MAKE IT INVISIBLE
	ImageView winView = guiImages.get(WIN_DISPLAY_TYPE);
	winView.setVisible(false);
        winRegion.setVisible(false);
        winScore.setVisible(false);
        winGameDuration.setVisible(false);
        winSubRegions.setVisible(false);
        winIncorrectGuess.setVisible(false);
        
	// AND RESET ALL GAME DATA
	data.reset(this);
    }

    /**
     * This mutator method changes the color of the debug text.
     *
     * @param initColor Color to use for rendering debug text.
     */
    public static void setDebugTextColor(Color initColor) {
//        debugTextColor = initColor;
    }

    /**
     * Called each frame, this method updates the rendering state of all
     * relevant GUI controls, like displaying win and loss states and whether
     * certain buttons should be enabled or disabled.
     */
    int backgroundChangeCounter = 0;

    @Override
   
    public void updateGUI() {
	// IF THE GAME IS OVER, DISPLAY THE APPROPRIATE RESPONSE
	if (data.won()) {
             ImageView winImage = guiImages.get(WIN_DISPLAY_TYPE);
             winImage.setVisible(true);
             ImageView map = guiImages.get(MAP_TYPE);
             map.setVisible(false);
             ImageView provinceimage = guiImages.get(PROVINCE_IMAGE);
             provinceimage.setVisible(false);
             RegionsFound.setVisible(false);
             RegionsLeft.setVisible(false);
             IncorrectGuesses.setVisible(false);
             ((RegioVincoDataModel)data).returnTimer().setVisible(false);
             winRegion.setVisible(true);
             winScore.setVisible(true);
             long lala=((RegioVincoDataModel)data).totalTime;
             long lalala=((RegioVincoDataModel)data).counter;
             long lalalala=10000-((lala*10)+(lalala*100));
             if(lalalala<0){
                 lalalala=0;
             }
             winScore.setText("Score:"+lalalala);
             String l=((RegioVincoDataModel)data).returnTimer().getText();
             winGameDuration.setText("Game Duration:"+ l);
             winGameDuration.setVisible(true);
             winSubRegions.setVisible(true);
             String j=IncorrectGuesses.getText();
             winIncorrectGuess.setText(j);
             winIncorrectGuess.setVisible(true);
        
         }  
             
    }

    public void reloadMap() {
	Image tempMapImage = loadImage(AFG_MAP_FILE_PATH);
	PixelReader pixelReader = tempMapImage.getPixelReader();
	WritableImage mapImage = new WritableImage(pixelReader, (int) tempMapImage.getWidth(), (int) tempMapImage.getHeight());
        for(int i=0;i<mapImage.getWidth();i++){
            for(int j=0;j<mapImage.getHeight();j++){
                Color a=pixelReader.getColor(i, j);
                if(a.equals(Color.rgb(220, 110, 0)))
                mapImage.getPixelWriter().setColor(i, j, Color.BLACK);
            }
        }
            
	ImageView mapView = guiImages.get(MAP_TYPE);
	mapView.setImage(mapImage);
	int numSubRegions = ((RegioVincoDataModel) data).getRegionsFound() + ((RegioVincoDataModel) data).getRegionsNotFound();
	this.boundaryTop = -(numSubRegions * 50);
	// AND GIVE THE WRITABLE MAP TO THE DATA MODEL
	((RegioVincoDataModel) data).setMapImage(mapImage);
    }
   
    public Label addGuiLabel(String type, Label label, int x, int y)
    {
        label=new Label(type);
        label.setScaleX(2);
        label.setScaleY(2);
        label.setTextFill(Color.web("white"));
        label.translateXProperty().setValue(x);
        label.translateYProperty().setValue(y);

        return label;
        
    }
     public Label addGuiLabel2(String type, Label label, int x, int y)
    {
        label=new Label(type);
        label.setScaleX(1.5);
        label.setScaleY(1.5);
        label.setTextFill(Color.web("blue"));
        label.translateXProperty().setValue(x);
        label.translateYProperty().setValue(y);

        return label;
        
    }
    public void controll(){
        
        RegionsFound.setText("Regions Found:"+((RegioVincoDataModel) data).getRegionsFound());
        RegionsLeft.setText("Regoins Left:"+((RegioVincoDataModel) data).getRegionsNotFound());
        IncorrectGuesses.setText("Incorrect Guesses:"+((RegioVincoDataModel) data).counter);
        
    }
    public Pane getGuiPane(){
        return guiLayer;
        
    }
}
